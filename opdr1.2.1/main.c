#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool isPrime(int n)
{
    if (n < 2)
    {
        return false;
    }
    int i;
    for ( i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }
    return true;
}

void maxmin(int n, int *k, int *g)
{
    if (n < 2)
    {
        return 0;
    }

    int i = n-1;
    while(*k == 0)
    {
        if(isPrime(i) == true)
        {
            *k = i;
        }
        i--;
    }
    i = n+1;
    while(*g == 0)
    {
        if(isPrime(i) == true)
        {
            *g = i;
        }
        i++;
    }
}

int main(void)
{
    int number;
    int kleiner = 0, groter = 0;
    printf("Geef een geheel number: ");
    scanf("%d", &number);
    printf("Het number %d is ", number);
    if (isPrime(number))
    {
        printf("een ");
    }
    else
    {
        printf("geen ");
    }
    printf("priemgetal\n");

    maxmin(number, &kleiner, &groter);
    printf("Het kleinste priemgetal groter dan %d is %d\n", number, groter);
    printf("Het grootste priemgetal kleiner dan %d is %d\n", number, kleiner);

    while (1);
    return 0;
}
