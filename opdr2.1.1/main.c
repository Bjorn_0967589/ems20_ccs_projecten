#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define AANTAL_SPELERS_IN_LIJST 3
#define MAX_AANTAL_KARAKTERS_IN_NAAM 15
#define TOP_AANTAL_SPELERS 5

typedef struct {
    char naam[MAX_AANTAL_KARAKTERS_IN_NAAM + 1];
    unsigned int punten;
} Speler;

void maakLijstLeeg(Speler lijst[], size_t aantalInLijst)
{
    for (size_t i = 0; i < aantalInLijst; i++)
    {
        size_t ci;
        for (ci = 0; ci < MAX_AANTAL_KARAKTERS_IN_NAAM; ci++)
        {
            lijst[i].naam[ci] = '-';
        }
        lijst[i].naam[ci] = '\0';
        lijst[i].punten = 0;
    }
}

void printLijst(Speler lijst[], size_t aantalInLijst)
{
    for (size_t i = 0; i < aantalInLijst; i++)
    {
        printf("%-*s %5d\n", MAX_AANTAL_KARAKTERS_IN_NAAM, lijst[i].naam, lijst[i].punten);
    }
}

void zetBovenaanInLijst(Speler s, Speler lijst[], size_t aantalInLijst)
{
    // Alle voorgaande spelers een plaatsje opschuiven:
    for (size_t i = aantalInLijst - 1; i > 0; i--)
    {
        lijst[i] = lijst[i - 1];
    }
    // De laatste speler komt bovenaan de lijst:
    lijst[0] = s;
}

int speelSpel(void)
{
    // Hier komt  de code die het spel bestuurt.
    // Het aantal behaalde punten wordt als returnwaarde teruggegeven.
    return rand();
}

void leesNaam(char s[])
{
    char karakter;
    size_t i = 0;
    do
    {
        karakter = getchar();
        if (isprint(karakter) && i != MAX_AANTAL_KARAKTERS_IN_NAAM)
        {
            s[i] = karakter;
            i++;
        }
    }
    while (karakter != '\n');
    s[i] = '\0';
}

void highscore(Speler s, Speler lijst[], size_t aantalInLijst)
{

    for(size_t i = 0; i < aantalInLijst; i++)
    {

        if(s.punten > lijst[i].punten)
        {
            for (size_t u = aantalInLijst - 1; u > i ; u--)
            {
                lijst[u] = lijst[u - 1];

            }
         //lijst[i + 1] = lijst[i];

         lijst[i] = s;
         break;
        }
    }
}

int main(void)
{
    srand(time(NULL));
    Speler laatsteSpelers[AANTAL_SPELERS_IN_LIJST];
    Speler ranglijst[TOP_AANTAL_SPELERS];
    maakLijstLeeg(laatsteSpelers, AANTAL_SPELERS_IN_LIJST);
    maakLijstLeeg(ranglijst, TOP_AANTAL_SPELERS);

    while (1)
    {
        Speler s;
        s.punten = speelSpel();
        printf("Je hebt %d punten behaald, type nu je naam in: ", s.punten);
        leesNaam(s.naam);
        zetBovenaanInLijst(s, laatsteSpelers, AANTAL_SPELERS_IN_LIJST);
        printf("Laatste %d spelers:\n", AANTAL_SPELERS_IN_LIJST);
        printLijst(laatsteSpelers, AANTAL_SPELERS_IN_LIJST);

        highscore(s, ranglijst, TOP_AANTAL_SPELERS);
        printf("Beste %d spelers:\n", TOP_AANTAL_SPELERS);
        printLijst(ranglijst, TOP_AANTAL_SPELERS);
    }
    return 0;
}
