#include <stdint.h>
#include <stddef.h>
#include "register_def.h"
//Extra includes hier onder
#include <gpio.h>
#include <pin.h>
#include <prcm.h>


int main(void)
{
    //Hier komt jouw code
    PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK);
//    HWREG(OCP_SHARED_BASE + OCP_SHARED_GPIO_PAD_CONFIG_9_MEM_GPIO_PAD_CONFIG_9_M) = 0x00000060;
//    HWREG(OCP_SHARED_BASE + OCP_SHARED_GPIO_PAD_CONFIG_10_MEM_GPIO_PAD_CONFIG_10_M) = 0x00000060;
//    HWREG(OCP_SHARED_BASE + OCP_SHARED_GPIO_PAD_CONFIG_11_MEM_GPIO_PAD_CONFIG_11_M) = 0x00000060;
//    HWREG(GPIOA1_BASE + GPIO_O_GPIO_DIR) = 14;
//    HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b00001110<<2)) = 2;
    PinTypeGPIO(PIN_01, PIN_STRENGTH_2MA, false);
    PinTypeGPIO(PIN_02, PIN_STRENGTH_2MA, false);
    GPIODirModeSet(GPIOA1_BASE, (GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3), GPIO_DIR_MODE_OUT);
    GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_1);



    while(1)
    {

    }
}
