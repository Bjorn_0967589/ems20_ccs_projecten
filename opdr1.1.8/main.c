/*
 * Copyright (C) 2018, Hogeschool Rotterdam
 * All rights reserved.
 */

#include <stdio.h>
#include <math.h>

int main ( void )
{
    int a, b, c, d;
    while (1){

        printf("Voer 4 gehele getallen in: \n");
        scanf("%d %d %d %d", &a, &b, &c, &d);
        float avg = (a+b+c+d)/4.0;
        printf("De gemiddelde waarden is: %.2f\n", avg);
        float sd = sqrt((pow((a-avg),2) + pow((b-avg), 2) + pow((c-avg), 2) + pow((d-avg), 2))/4.0);
        printf("De standaard afwijking: %.2f\n", sd);


    }
    return 0;
}
