#include <stdio.h>
#include <string.h>

size_t strlen_met_index(char *s)
{
    size_t i = 0;

    while (s[i] != '\0')
    {
        i++;
    }
    return i;
}

size_t strlen_met_pointer(char *s)
{
    char *p = s;

    while (*p != '\0')
    {
        p++;
    }
    return p - s;
}

int main(void)
{
    char* test = "Hoe lang is deze string?";
    size_t len1 = strlen_met_index(test);
    size_t len2 = strlen_met_pointer(test);
    size_t len3 = strlen(test);
    if (len1 != len3)
    {
        printf("str_len_met_index() werkt niet goed!\n");
    }
    else
    {
        printf("str_len_met_index() werkt naar verwachting\n");
    }
    if (len2 != len3)
    {
        printf("str_len_met_pointer() werkt niet goed!\n");
    }
    else
    {
        printf("str_len_met_pointer() werkt naar verwachting\n");
    }

    while (1);
    return 0;
}
