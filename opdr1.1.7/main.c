/*
 * Copyright (C) 2018, Hogeschool Rotterdam
 * All rights reserved.
 */

#include <stdio.h>

int main ( void )
{
    int i = 51966;
    char c = '?';
    double d = 3.141592653;
    char s [] = "Dit is een string !";

    // Vul hier je code in
    printf("\n De variabele i heeft de waarde: %d\n In hexadecimale notatie is dit: %x\n De variabele c heeft de waarde: %c\n De variabele d heeft de waarde: %.8f\n"
            " De variabele s heeft de waarde: %s\n", i, i, c, d, s);

    while (1) ;
    return 0;
}
