#include <stdio.h>

typedef struct {
    int teller;
    int noemer;
} Breuk;

int ggd(int n, int m)
{
    if (m == 0)
    {
        return n;
    }
    else
    {
        return ggd(m, n % m);
    }
}

Breuk normaliseer(Breuk b)
{
    int d = ggd(b.teller, b.noemer);
    b.teller /= d;
    b.noemer /= d;
    if (b.noemer < 0)
    {
        b.noemer = -b.noemer;
        b.teller = -b.teller;
    }
    return b;
}

Breuk product(Breuk b1, Breuk b2)
{
    Breuk p;
    p.teller = b1.teller * b2.teller;
    p.noemer = b1.noemer * b2.noemer;
    return normaliseer(p);
}

Breuk som(Breuk a1, Breuk a2)
{
    Breuk t;
    Breuk n;
    Breuk p;
    if(a1.noemer != a2.noemer)
    {
        t.noemer = a1.noemer * a2.noemer;
        t.teller = a1.teller * a2.noemer;

        //n.noemer = a2.noemer * a1.noemer;
        n.teller = a2.teller * a1.noemer;

        p.teller = t.teller + n.teller;
        p.noemer = t.noemer;
    }
    else
    {
        p.teller = a1.teller + a2.teller;
        p.noemer = a1.noemer;
    }
    return normaliseer(p);
}

int main(void)
{
    Breuk a = {-7, 14}, b = {2, -3};

    Breuk c = som(a, b);
    printf("c = %d/%d\n", c.teller, c.noemer);

    Breuk d = product(a, b);
    printf("d = %d/%d\n", d.teller, d.noemer);

    while (1);
    return 0;
}
