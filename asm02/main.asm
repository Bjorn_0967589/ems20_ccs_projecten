;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

		; Set DCO to 1 MHz:
		clr.b	&DCOCTL					; Select lowest DCOx
										; and MODx settings
		mov.b	&CALBC1_1MHZ, &BCSCTL1	; Set range
		mov.b	&CALDCO_1MHZ, &DCOCTL	; Set DCO step + modulation

		bis.b	#0x40, &P1DIR	; Set de pin 1 op poort 1 als uitgang
loop	xor.b	#0X40, &P1OUT	; Zorgt ervoor dat pin 1 aan en uit toggelt
		mov.w	#25000, r15		; Geeft register r15 een waarde van 50000
telaf	dec.w	r15				; Trekt steeds 1 af van de waarde van r15
		jnz		telaf			; Jumpt naar telaf wanneer statusbit Z nul is, dus wanneer de waarde van r15 niet gelijk is aan 0
								; Dit zorgt ervoor dat ledlampje 50000 klokcycles aan of uit blijft
		jmp		loop			; jumpt naar loop
                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
