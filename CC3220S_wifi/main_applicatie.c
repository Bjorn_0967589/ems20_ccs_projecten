// C header files
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
// Driver Header files
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>
// RTOS header files
#include <ti/sysbios/BIOS.h>
// POSIX Header files
#include <ti/posix/ccs/pthread.h>
#include <ti/posix/ccs/semaphore.h>

// Eigen headers
#include "uart_term.h"
#include "bestanden.h"

// Taken
void *App_Task(void *args);

// Functies
void PingResultaat(SlNetAppPingReport_t *pReport);
int StelWifiIn(SlWlanSecParams_t *SecParams, SlWlanSecParamsExt_t *SecExtParams, _i8 *wachtwoord, bool vraagOmWachtwoord);
void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args));

// Semaphore voor taaksynchronisatie
sem_t wachtenOpIP;

// Wifi instellingen
#define IDENTITY "0967589@hr.nl"
#define SSID "ems20"

void *mainThread(void *arg0)
{
    // Variabelen
    int Status;
    SlWlanSecParams_t SecParams; // Wifi parameters
    SlWlanSecParamsExt_t SecExtParams; // Wifi enterprise parameters
    _i8 ww[64]; // Buffer om een wifi-wachtwoord in op te slaan

    // Intialisaties
    sem_init(&wachtenOpIP, 0, 0); // Semaphore voor synchronisatie
    SPI_init();
    InitTerm(); // UART functionaliteit van TI

    // Prio stack functie
    MaakTaak(9, 2048, sl_Task); // Nodig voor wifi api
    MaakTaak(2, 2048, App_Task); // Onze aplicatie

    // Hello
    printf("\nApplicatie gestart.\nOverige meldingen zijn te vinden op de UART terminal op 115200 baud.\n");
    UART_PRINT("Starten.\r\n");

    bool vraagOmWachtwoord = false; // Probeer eerst om opgeslagen wachtwoord te gebruiken
    do
    {
        // Wifi instellen en verbinden
        StelWifiIn(&SecParams, &SecExtParams, ww, vraagOmWachtwoord);

        Status = sl_WlanConnect((_i8*)SSID, // SSID
                                strlen(SSID), // Lengte van SSID
                                0,
                                &SecParams, // Security parameters wifi
                                0); // Extended security parameters enterprice wifi
        if (Status !=0)
        {
            UART_PRINT("Wifi instellingen fout: %d\r\n", Status);
            vraagOmWachtwoord = true; // Probeer nogmaals om een correct wachtwoord in te voeren
        }
    }
    while (Status != 0);

    /*!
     * De thread kan nu stoppen.
     * Wachten op succesvolle verbinding zodat de App-thread
     * verder kan gaan.
     *
     * @see: SimpleLinkNetAppEventHandler(),
     * @see: App_Task(void * args)
     *
     */
    return 0;
}


void *App_Task(void *args)
{
    _u16 len = sizeof(SlNetCfgIpV4Args_t);
    _u16 ConfigOpt = 0;
    _u32 googleIP;
    SlNetCfgIpV4Args_t ipV4 = {0}; // Struct voor ip
    SlNetAppPingReport_t report; // Struct voor ping
    SlNetAppPingCommand_t pingCommand; // Struct voor ping

    /* Wachten op een ip adres, daarna kunnen we verder.
     * Zie ook SimpleLinkNetAppEventHandler() */
    sem_wait(&wachtenOpIP);

    // Wachtwoord alleen opslaan als verbinden gelukt is.
    schrijfWachtwoord();

    // Haal IP op
    sl_NetCfgGet(SL_NETCFG_IPV4_STA_ADDR_MODE,
                 &ConfigOpt,
                 &len,
                 (_u8 *)&ipV4
                );

    // Pretty print IP
    UART_PRINT( "\r\n"
                "Mijn IP\t %d.%d.%d.%d\r\n"
                "MASK\t %d.%d.%d.%d\r\n"
                "GW\t %d.%d.%d.%d\r\n"
                "DNS\t %d.%d.%d.%d\r\n",
                SL_IPV4_BYTE(ipV4.Ip,3), SL_IPV4_BYTE(ipV4.Ip,2), SL_IPV4_BYTE(ipV4.Ip,1), SL_IPV4_BYTE(ipV4.Ip,0),
                SL_IPV4_BYTE(ipV4.IpMask,3), SL_IPV4_BYTE(ipV4.IpMask,2), SL_IPV4_BYTE(ipV4.IpMask,1), SL_IPV4_BYTE(ipV4.IpMask,0),
                SL_IPV4_BYTE(ipV4.IpGateway,3), SL_IPV4_BYTE(ipV4.IpGateway,2), SL_IPV4_BYTE(ipV4.IpGateway,1), SL_IPV4_BYTE(ipV4.IpGateway,0),
                SL_IPV4_BYTE(ipV4.IpDnsServer,3), SL_IPV4_BYTE(ipV4.IpDnsServer,2), SL_IPV4_BYTE(ipV4.IpDnsServer,1), SL_IPV4_BYTE(ipV4.IpDnsServer,0));

    // DNS request naar google.nl, wat is het IP?
    sl_NetAppDnsGetHostByName("www.google.nl", strlen("www.google.nl"), &googleIP, SL_AF_INET);

    //Wat willen we pingen?
    pingCommand.Ip = SL_IPV4_VAL(192,168,0,107);
    pingCommand.PingSize = 32; // Size of ping, in bytes
    pingCommand.PingIntervalTime = 100; // Delay between pings, in milliseconds
    pingCommand.PingRequestTimeout = 1000; // Timeout for every ping in milliseconds
    pingCommand.TotalNumberOfAttempts = 4; // Max number of ping requests. 0 - forever
    pingCommand.Flags = 0; // Report only when finished

    // Feedback dat we starten met de ping operatie
    UART_PRINT("\r\nping www.google.nl (%d.%d.%d.%d)\r\n",
               SL_IPV4_BYTE(googleIP,3),
               SL_IPV4_BYTE(googleIP,2),
               SL_IPV4_BYTE(googleIP,1),
               SL_IPV4_BYTE(googleIP,0));


    // Ping uitvoeren
    sl_NetAppPing(&pingCommand, SL_AF_INET, &report, PingResultaat);


    // Deze thread is klaar. Nu wachten op PingResultaat.
    return NULL;
}

// Callback van ping functie
void PingResultaat(SlNetAppPingReport_t* pReport)
{
    // Resultaten afdrukken.
    UART_PRINT("Verzonden: %d, "
               "Ontvangen: %d\r\n"
               "Gemiddelde Responsietijd: %dms\r\n",
               pReport->PacketsSent,
               pReport->PacketsReceived,
               pReport->AvgRoundTime);

    // Klaar met ping applicatie
}

void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args))
{
    pthread_t Thread;
    pthread_attr_t attrs;
    struct sched_param priParam;
    int retc;

    // sl_task thread maken. Dit handelt alle callbacks van wifi af
    priParam.sched_priority = prioriteit;
    retc = pthread_attr_init(&attrs);
    retc |= pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED); // Ontkoppelde taak
    retc |= pthread_attr_setschedparam(&attrs, &priParam); // Prioriteit instellen
    retc |= pthread_attr_setstacksize(&attrs, stackSize); // Stacksize instellen

    retc |= pthread_create(&Thread, &attrs, functie, NULL); // Taak starten
    if (retc != 0)
    {
        // pthread_create() failed
        UART_PRINT("Taak fout: %d",retc);
        while (1);
    }
}

int StelWifiIn(SlWlanSecParams_t* SecParams, SlWlanSecParamsExt_t* SecExtParams, _i8* wachtwoord, bool vraagOmWachtwoord)
{
    int Status;
    SlDateTime_t dateTime = {0};
    _u8 serverAuthenticatie = 0;

    // NWP starten en configureren
    sl_Start(0, 0, 0);
    Status = sl_WlanSetMode(ROLE_STA); // Station modus

    /* Om te voorkomen dat jouw wachtwoord in je code is te vinden wordt het
     * geschreven naar een bestand op de flash.
     *
     * Je moet eenmalig het wachtwoord geven, elke keer daarna kun je het
     * alleen uitlezen. Als je het wilt aanpassen dan moet parameter vraagOmWachtwoord
     * true worden.
     * */
    leesWachtwoord((char *)wachtwoord, vraagOmWachtwoord);

    SecParams->Key = wachtwoord;
    SecParams->KeyLen = strlen((const char*)wachtwoord);
    SecParams->Type = SL_WLAN_SEC_TYPE_WPA_WPA2;

    // extended alleen nodig voor enterprise wifi
    SecExtParams->User = IDENTITY;
    SecExtParams->UserLen = strlen(IDENTITY);
    SecExtParams->AnonUser = 0;
    SecExtParams->AnonUserLen = 0;
    SecExtParams->EapMethod = SL_WLAN_ENT_EAP_METHOD_PEAP0_MSCHAPv2;

    // Jaar instellen zodat het certificaat niet wordt afgekeurd
    dateTime.tm_year =  (_u32)2019; // Year (YYYY format)

    // NWP herstarten om STA modus ook te gebruiken
    sl_Stop(0);
    sl_Start(0, 0, 0);

    // Tijd opslaan
    sl_DeviceSet(SL_DEVICE_GENERAL,
                 SL_DEVICE_GENERAL_DATE_TIME,
                 sizeof(SlDateTime_t),
                 (_u8 *)(&dateTime));

    // Eduroam geeft geen CA certificaat uit, dus check uitzetten
    Status = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, SL_WLAN_GENERAL_PARAM_DISABLE_ENT_SERVER_AUTH, 1, &serverAuthenticatie);

    return Status;
}
