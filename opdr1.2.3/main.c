#include <stdio.h>
#include <string.h>

void keerom_met_indexen(char *s)
{
    size_t eerste = 0;
    size_t laatste = strlen(s) - 1;

    while (laatste > eerste)
    {
        char hulp = s[eerste];
        s[eerste] = s[laatste];
        s[laatste] = hulp;
        eerste++;
        laatste--;
    }
}

void keerom_met_pointers(char *s)
{
    char *p_eerste = s;
    char *p_laatste = s + strlen(s) - 1;

    while(p_laatste > p_eerste)
    {
        char hulp = *p_eerste;
        *p_eerste = *p_laatste;
        *p_laatste = hulp;
        p_eerste++;
        p_laatste--;
    }

    // vul hier je code in
}

int main(void)
{
    char test1[] = "Keer dit eens om";
    printf("\"%s\" omgekeerd is ", test1);
    keerom_met_indexen(test1);
    printf("\"%s\"\n", test1);

    char test2[] = "Keer dit eens om";
    printf("\"%s\" omgekeerd is ", test2);
    keerom_met_pointers(test2);
    printf("\"%s\"\n", test2);

    while (1);
    return 0;
}
