#include <stdint.h>
#include <stddef.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC32XX.h>

/* Example/Board Header files */
#include "Board.h"

/*
 *  ============= mainThread ===============
 *
 *  Dit is de 'main' functie van het project
 *  Debugger is ingesteld om hier te te pauzeren
 *  i.p.v. bij de originele main-functie
 *
 *  Tips:
 *  1. Controleer de 'Linked Resources'
 *     onder 'project properties' om
 *     compilerproblemen te voorkomen
 *
 *  2. Gebruik 'Board.html' uit het project om
 *     de pinnamen te zien van de driver
 *
 */

void *mainThread(void *arg0)
{
    /*
     *  code komt hier
     *
     */
    bool status;
    I2C_init();
    I2C_Handle i2cHandle;

    i2cHandle = I2C_open(0, NULL);

    if (i2cHandle == NULL)
    {
        // Error opening I2C
        while(1) {}
    }

    I2C_Transaction i2cTransaction = {0};
    uint8_t readBuffer[5];
    uint8_t writeBuffer[1];

    writeBuffer[0] = 0xFE;

    i2cTransaction.slaveAddress = 0x41;
    i2cTransaction.writeBuf = writeBuffer;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf = readBuffer;
    i2cTransaction.readCount = 5;


     status = I2C_transfer(i2cHandle, &i2cTransaction);

    if (status == false)
    {
        // Unsuccesful I2C transfer
    }

//    GPIO_init();
//    while (1)
//    {
//        GPIO_toggle(Board_GPIO_LED0);
//    }


}
