#include <stdio.h>

enum component {R, C, L};

typedef struct {
    int type;
    int index;
    double waarde;
} PassieveComponent;

void printComponent(PassieveComponent c)
{
    switch (c.type)
    {
    case R:
        printf("R%d = %.1E ohm", c.index, c.waarde);
        break;
    case C:
        printf("C%d = %.1E farad", c.index, c.waarde);
        break;
    case L:
        printf("L%d = %.1E henry", c.index, c.waarde);
        break;
    }
}

int main(void)
{
    PassieveComponent c[] = {
        {R, 1, 1E6}, {R, 2, 2.7E3}, {C, 1, 1E-9}, {L, 1, 1E-3}, {C, 2, 15E-6}
    };

    printf("\n");
    for (size_t i = 0; i < sizeof c / sizeof c[0]; i++)
    {
        printComponent(c[i]);
        printf("\n");
    }

    while (1);
    return 0;
}
